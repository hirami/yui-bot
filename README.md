[Yui Bot](https://imgur.com/HLOBiOx) 

What this is
---

In a nutshell, this package provides you with a fancy way of linking objects together called "Dependency Injection" (DI). The goal of this package is to make DI portable so it can be used inside libraries too and not only applications.
This library was heavily inspired by [Angular's](https://github.com/angular/angular) way of doing DI so some terms may sound familiar.

Installation
---

With NPM

```
npm i @yui
```

With Yarn

```
yarn add @yui
```

Usage
---

Soon™ (Meanwhile, there are some examples inside the Injector.spec.ts file and inside the @ayana/test README)

Testing with [@yui/test](https://www.npmjs.com/package/@ayana/test)
---

See the [README of the @ayana/test module](https://www.npmjs.com/package/@ayana/test#testing-with-ayanadi) for more information about how to test software that uses @ayana/di.

Links
---

[GitLab repository](https://gitlab.com/ayana/libs/di)

[NPM package](https://npmjs.com/package/@ayana/di)

License
---

Refer to the [LICENSE](LICENSE) file.
